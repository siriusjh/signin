<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>Insert title here</title>
</head>
<body>
	<div class="row">
		<div class="col-md-10">
			<div>
				<span id="login-user">사용자</span>  ${userName}님, 안녕하세요.
				 <a href="/test" class="btn btn-sm btn-info active">Logout</a>
			</div>
			<div>
				<!-- 스프링 시큐리티에서 기본 제공하는 URL - 별도 컨트롤러 작성 필요 없음 -->
				<a href="/oauth2/authorization/google"
					class="btn btn-sm btn-success active" role="button">Google
					Login</a>
			</div>
		</div>
	</div>
	
<script type="text/javascript">
var logout = function() {
    document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://localhost:8080";
}

</script>	
</body>
</html>