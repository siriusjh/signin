package com.example.awsboard.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ResolvableType;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.awsboard.cofig.auth.LoginUser;
import com.example.awsboard.cofig.auth.dto.SessionUser;

@Controller
public class IndexController {

	private HttpSession httpSession;

	private static final String authorizationRequestBaseUri = "oauth2/authorization";
	Map<String, String> oauth2AuthenticationUrls = new HashMap<>();

	@Autowired
	private ClientRegistrationRepository clientRegistrationRepository;
	// Lombok 아닌 경우 (@RequiredArgsConstructor 없는 경우)
	// @Autowired private ClientRegistrationRepository clientRegistrationRepository;

	@SuppressWarnings("unchecked")
	//@GetMapping("/login")
	public String getLoginPage(Model model) throws Exception {

		Iterable<ClientRegistration> clientRegistrations = null;
		ResolvableType type = ResolvableType.forInstance(clientRegistrationRepository).as(Iterable.class);
		if (type != ResolvableType.NONE && ClientRegistration.class.isAssignableFrom(type.resolveGenerics()[0])) {
			clientRegistrations = (Iterable<ClientRegistration>) clientRegistrationRepository;
		}

		assert clientRegistrations != null;
		clientRegistrations.forEach(registration -> oauth2AuthenticationUrls.put(registration.getClientName(),
				authorizationRequestBaseUri + "/" + registration.getRegistrationId()));
		model.addAttribute("urls", oauth2AuthenticationUrls);

		return "auth/oauth-login";
	}

	@GetMapping("/")
	public String index(Model model, @LoginUser SessionUser user) {
		// .............
		// 사용자 정보: 위의 @LoginUser 어노테이션으로 대체
		//SessionUser user = (SessionUser) httpSession.getAttribute("user");
		if (user != null) {
			model.addAttribute("userName", user.getName());
			model.addAttribute("userImg", user.getPicture());
			return "main";
		}
		return "hello";
	}
	
	
	//@ResponseBody
	@GetMapping("/test")
	public String logout(HttpServletRequest httpRequest, HttpSession session, HttpServletResponse response) throws IOException {
		//String reDirectPage = "";
       // reDirectPage="https://www.google.com/accounts/Logout?https://appengine.google.com/_ah/logout?http://localhost:8080";
      //  reDirectPage="http://localhost:8080";
	    session.removeAttribute("user");
	   // response.sendRedirect(reDirectPage);
	    //curl -H "Content-type:application/x-www-form-urlencoded" https://accounts.google.com/o/oauth2/revoke?token=ya29.a0AfH6SMB-_rCBKKNEq8jeqRWH7jF27gjlA6zGv_lsv7Fqh1zRbMH0jamsFXMINCh8RFIvhsqgkId7H-lQsmETf5T-nwsnfERWzQINolGEzF6haagYp-GlVCP7-iSdxBxCgSUyfT01HeRa4QwzmVeLqIoO-p_Khqn-JEc
	    return "hello";
	}
	
	
	@GetMapping("/common/test")
	public String test(HttpServletRequest httpRequest, HttpSession session, HttpServletResponse response) throws IOException {
	
	    return "auth";
	}

	
	@GetMapping("/den")
	public String error(HttpServletRequest httpRequest, HttpSession session, HttpServletResponse response) throws IOException {
	
	    return "auth";
	}
}
