package com.example.awsboard.cofig.auth;

import java.util.Map;

import com.example.awsboard.domain.user.User;

public class OAuthAttributes {

	private Map<String, Object> attributes;
	private String nameAttributeKey, name, email, picture;

	public OAuthAttributes(Map<String, Object> attributes, String nameAttributeKey, String name, String email,
			String picture) {
		this.attributes = attributes;
		this.nameAttributeKey = nameAttributeKey;
		this.name = name;
		this.email = email;
		this.picture = picture;
	}

	public static OAuthAttributes of(String registrationId, String userNameAttributeName,
			Map<String, Object> attributes) {
		OAuthAttributes oAuthAttributes = new OAuthAttributes(attributes, userNameAttributeName,
				(String) attributes.get("name"), (String) attributes.get("email"), (String) attributes.get("picture"));
		return oAuthAttributes;

	}
	
	public User toEntity() {
		User user = new User(name, email, picture, "ROLE_USER");
		return user;
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	public String getNameAttributeKey() {
		return nameAttributeKey;
	}

	public void setNameAttributeKey(String nameAttributeKey) {
		this.nameAttributeKey = nameAttributeKey;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}


}
