package com.example.awsboard.cofig.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
    private CustomOAuth2UserService customOAuth2UserService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .headers().frameOptions().disable()
                
            .and()
                .authorizeRequests()
                .antMatchers("/", "/css/**", "/images/**", "/js/**", "/h2/**", "/h2-console/**").permitAll()
              // .anyRequest().access("@authorizationChecker.check(request, authentication)")
                .anyRequest().authenticated()
				/*
				 * .and() .logout().logoutSuccessUrl("/")
				 */
                .and()
                .oauth2Login().userInfoEndpoint().userService(customOAuth2UserService);
           /* .and()
            	.exceptionHandling()
            	.authenticationEntryPoint( new AuthenticationEntryPoint() {

                         @Override
                         public void commence(HttpServletRequest request, HttpServletResponse response,
                                     AuthenticationException authException) throws IOException, ServletException {
                                     response.sendRedirect("/");
                          }
               })
               .accessDeniedHandler( new AccessDeniedHandler() {

                        @Override
                        public void handle(HttpServletRequest request, HttpServletResponse response,
                                    AccessDeniedException accessDeniedException) throws IOException, ServletException {
                                    response.sendRedirect("/denied");
                        }
              })
              ;*/
                
    }

}