package com.example.awsboard.cofig.auth;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import com.example.awsboard.domain.user.User;
import com.example.awsboard.domain.user.UserRepository;
import com.example.awsboard.mapper.MenuMapper;

@Component
public class AuthorizationChecker {
    @Autowired
    private MenuMapper menuMapper;
 
    @Autowired
    private UserRepository userRepository;
 
    //걍 interceptor에 추가
    
    public boolean check(HttpServletRequest request, Authentication authentication) {
    	
        Object principalObj = authentication.getPrincipal();
 
		/*
		 * if (!(principalObj instanceof User)) { return false; }
		 */
    
        //session 에서 email 뽑을 것
        //request 에서 url 뽑을 것
        Map<String, Object> menu = menuMapper.getRoleMenu();
		
		 if(ObjectUtils.isEmpty(menu)) { return true; }
		 
        String roleType = (String) menu.get("ROLE_TYPE");
        String writeFlag = (String) menu.get("write_flag");
        System.out.println(roleType);
        System.out.println(writeFlag);
 
        if (roleType.equals("read") && writeFlag.equals("Y")) {
            return false;
        } else if(roleType.equals("lock")) {
        	return false;
        }
        return true;
    }
}
