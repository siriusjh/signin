-- --------------------------------------------------------
-- 호스트:                          127.0.0.1
-- 서버 버전:                        10.5.6-MariaDB - mariadb.org binary distribution
-- 서버 OS:                        Win64
-- HeidiSQL 버전:                  11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- entertainment 데이터베이스 구조 내보내기
CREATE DATABASE IF NOT EXISTS `entertainment` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `entertainment`;

-- 테이블 entertainment.menu 구조 내보내기
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `write_flag` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 테이블 데이터 entertainment.menu:~2 rows (대략적) 내보내기
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`, `url`, `name`, `write_flag`) VALUES
	(1, '/admin/read', NULL, 'N'),
	(2, '/admin/write', NULL, 'Y'),
	(3, '/common/test', NULL, 'Y');
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;

-- 테이블 entertainment.role_group 구조 내보내기
CREATE TABLE IF NOT EXISTS `role_group` (
  `ROLE_ID` varchar(50) DEFAULT NULL,
  `ROLE_NAME` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 entertainment.role_group:~3 rows (대략적) 내보내기
/*!40000 ALTER TABLE `role_group` DISABLE KEYS */;
INSERT INTO `role_group` (`ROLE_ID`, `ROLE_NAME`) VALUES
	('ROLE_USER', '사용자'),
	('ROLE_ADMIN', '관리자'),
	('ROLE_SUPER', '슈퍼');
/*!40000 ALTER TABLE `role_group` ENABLE KEYS */;

-- 테이블 entertainment.role_menu 구조 내보내기
CREATE TABLE IF NOT EXISTS `role_menu` (
  `MENU_ID` int(11) DEFAULT NULL,
  `ROLE_ID` varchar(50) DEFAULT NULL,
  `ROLE_TYPE` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 테이블 데이터 entertainment.role_menu:~9 rows (대략적) 내보내기
/*!40000 ALTER TABLE `role_menu` DISABLE KEYS */;
INSERT INTO `role_menu` (`MENU_ID`, `ROLE_ID`, `ROLE_TYPE`) VALUES
	(1, 'ROLE_ADMIN', 'read'),
	(2, 'ROLE_ADMIN', 'read'),
	(3, 'ROLE_ADMIN', 'read'),
	(1, 'ROLE_USER', 'lock'),
	(2, 'ROLE_USER', 'lock'),
	(3, 'ROLE_USER', 'read'),
	(1, 'ROLE_SUPER', 'write'),
	(2, 'ROLE_SUPER', 'write'),
	(3, 'ROLE_SUPER', 'write');
/*!40000 ALTER TABLE `role_menu` ENABLE KEYS */;

-- 테이블 entertainment.user 구조 내보내기
CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- 테이블 데이터 entertainment.user:~1 rows (대략적) 내보내기
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `email`, `name`, `picture`, `role`) VALUES
	(5, 'workjihyun@gmail.com', 'Jihyum Park', 'https://lh4.googleusercontent.com/-tHFXxaK1OLQ/AAAAAAAAAAI/AAAAAAAAAAA/AMZuucnoBlQc3lLeaJG99h0FlVZLA-pqBw/s96-c/photo.jpg', 'ROLE_USER');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
